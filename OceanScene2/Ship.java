package ekokPractice1;

import uwcse.graphics.*;
import uwcse.animation.*;
import java.awt.Color;

public class Ship implements Actor {
	// Not actually a ship
	
	private Polygon metronome;
	
	private int rtime = 0; // Time elapsed
	private double rdelta = 0.0; // Acceleration
	private final double R_VELOCITY = 1; // Top speed in px/frame
	private final double R_TICK = 20; // How many frames it takes to reach pi (one half-cycle)
	
	public Ship() {
		
		// Create our metronome for testing purposes
		metronome = new Polygon(Color.BLACK, true);
		metronome.addPoint(240, 200);
		metronome.addPoint(260, 200);
		metronome.addPoint(260, 50);
		metronome.addPoint(240, 50);
	}
	
	public void addTo(GWindow gw) {
		
		// Simple wrapping
		metronome.addTo(gw);
	}
	
	public void removeFromWindow() {
		
		// Simple wrapping
		metronome.removeFromWindow();
	}
	
	public void doAction(Stage stage) {
		
		rtime++; // Increment stage time
		
		rdelta = Math.cos((float)(rtime) / R_TICK); // Change velocity on cosine wave
		
		metronome.rotateAround(250, 200, rdelta * R_VELOCITY); // Rotate by velocity
	}
}
