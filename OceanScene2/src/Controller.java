import java.util.EnumMap;
import java.util.Set;
import uwcse.graphics.*;

public class Controller extends GWindowEventAdapter {
	
	// Registered controllers
	private static Set<Controller> runningControllers;
	
	// directional controls
	public static enum buttons {
		UP, DOWN, LEFT, RIGHT, FIRE, LOCKON
	}
	private EnumMap<buttons,Character> controls;
	
	public Controller() {
		// this.controls = new EnumMap<>(controls);
		//this.controls.put(buttons.UP, );
		// Finish implementing later once I've looked into maps more
	}
	
	public Controller(EnumMap<buttons,Character> controlScheme) {
		this.controls = new EnumMap<>(controlScheme);
	}
	
	// Test script to get scancodes.
	// ...Why are all directional keys returning 2^16-1?
	// It looks like it's limited to 7-bit ASCII. DEL is 127
	public void keyPressed(GWindowEvent e) {
		System.out.println((int)(e.getKey()));
	}
}
