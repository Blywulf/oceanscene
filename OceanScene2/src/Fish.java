

	import java.awt.Color;

import uwcse.graphics.*;
	/**
	 * A fish in a graphics window
	 */

	public class Fish {

		// The graphics window the fish belongs to
		private GWindow window;

		// The location of the fish
		private int x;

		private int y;

		// Scale of the drawing of the fish
		private double scale;
		
		// The direction of the fish
		private int direction;
		public static final int MOVE_RIGHT = 1;
		public static final int MOVE_LEFT = 0;
		
		
		//The shapes I use
		private Oval body,eye;
		private Triangle tail;
		private Line mouth;

		/**
		 * Draws a Fish in a graphics window
		 * 
		 * @param x
		 *            the x coordinate of the location of the fish
		 * @param y
		 *            the y coordinate of the location of the fish
		 * @param scale
		 *            the scale of the drawing of the fish
		 * @param window
		 *            the graphics window the fish belongs to
		 */
		public Fish(int x, int y, double scale, int direction, GWindow window) {
			// Initialize the instance fields
			this.x = x;
			this.y = y;
			this.scale = scale;
			this.direction = direction;
			
			this.window = window;
			
		// Draw the fish
		// direction: 1 = right, 0 = left
			if (direction == MOVE_RIGHT ){
				rightFaceFish();	}
			else {
				leftFaceFish();
		}
	}

		/**
		 * Draws this fish
		 */
		//private void drawfish(int direction) {
			
//			if (direction == 1){
//				rightFaceFish();
//			}
//			else {
//				leftFaceFish();
//			}
//		}
			
		
		public void rightFaceFish(){
			int r = (int)(Math.random() * 256);//random float red value
			int g = (int)(Math.random() * 256);//random float green value
			int b = (int)(Math.random() * 256);	//random float blue value
			Color fishColor = new Color(r,g,b);
			int pxscale = 50;
			int width = (int) (this.scale * pxscale);
			int height = (int) (0.3 * width);
			//Draw the body
			body = new Oval(x,y,width,height,fishColor,true);
			window.add(body);
			int cx = body.getX();
			int cy = body.getY();
			int cw = body.getWidth();
			int ch = body.getHeight();
			
			//Draw the tail
			int vertexAX = cx-(cw/3);
			int vertexAY = cy;
			int vertexBX = cx;
			int vertexBY = (cy+ch/2);
			int vertexCX = (int) (cx-cw/3);
			int vertexCY = (cy+ch);
			tail = new Triangle(vertexAX, vertexAY, vertexBX, vertexBY, vertexCX, vertexCY, fishColor, true);
			
			window.add(tail);
			
			//Draw the eye
			int eyeWidth = (int)(4*scale);
			int eyeHeight = (int)(4*scale);
			int eyeX = (int) (x +(3.0/4*cw));
			int eyeY = (int) (y +(1.0/3*ch));
			eye = new Oval( eyeX, eyeY, eyeWidth,eyeHeight,Color.BLACK,true);
			eye.moveTo(eyeX, eyeY);
			window.add(eye);
			
			//Draw the mouth
			int mouthX1 = eyeX;
			int mouthY1 = (int)(eyeY+eyeHeight+1);
			int mouthX2 = (int)(cx+cw);
			int mouthY2 = (int)(mouthY1);
			mouth = new Line(mouthX1,mouthY1,mouthX2,mouthY2);
			window.add(mouth);
					
			}
		public void leftFaceFish(){
			int r = (int)(Math.random() * 256);//random float red value
			int g = (int)(Math.random() * 256);//random float green value
			int b = (int)(Math.random() * 256);	//random float blue value
			Color fishColor = new Color(r,g,b);
			int pxscale = 50;
			int width = (int) (this.scale * pxscale);
			int height = (int) (0.3 * width);
			//Draw the body
			body = new Oval(x,y,width,height,fishColor,true);
			window.add(body);
			int cx = body.getX();
			int cy = body.getY();
			int cw = body.getWidth();
			int ch = body.getHeight();
			
			//Draw the tail
			int vertexAX = cx + cw;
			int vertexAY = (cy + ch/2 );
			int vertexBX = (cx + cw + cw/4);
			int vertexBY = (cy+ch);
			int vertexCX = (cx + cw + cw/4);
			int vertexCY = (cy);
			tail = new Triangle(vertexAX, vertexAY, vertexBX, vertexBY, vertexCX, vertexCY, fishColor, true);
			
			window.add(tail);
			
			//Draw the eye
			int eyeWidth = (int)(4*scale);
			int eyeHeight = (int)(4*scale);
			int eyeX = (int)(cx +cw/6);
			int eyeY = (int)(cy +(1.0/3*ch));
			eye = new Oval( eyeX, eyeY, eyeWidth,eyeHeight,Color.BLACK,true);
			eye.moveTo(eyeX, eyeY);
			window.add(eye);
			
			//Draw the mouth
			int mouthX1 = (int)(eyeX+10);
			int mouthY1 = (int)(eyeY + eyeHeight  +2);
			int mouthX2 = (int) cx;
			int mouthY2 = (int)(mouthY1);
			mouth = new Line(mouthX1,mouthY1,mouthX2,mouthY2);
			window.add(mouth);
			
		}
		// Make the mover
		private void remove() {
			//removes from window
			window.remove(body);
			window.remove(eye);
			window.remove(tail);
			window.remove(mouth);
		}
		public void swim() {
			int movementX = 10;
			int movementY = 10;
			if(direction==MOVE_RIGHT){ 
				this.x += movementX;
				remove();
				rightFaceFish();

			}
			if(direction==MOVE_LEFT){  //facing right, coordinates become greater.
				this.x -= movementX ;
				remove();
				leftFaceFish();
			}
			
			if(x==0||y==130){      //not swimming out of the ocean.
				direction = MOVE_RIGHT;
				remove();
				
				rightFaceFish();
				
			
			}
			else if(x==window.getWindowWidth() || y==window.getWindowHeight()){ //not going out of the window
				direction = MOVE_LEFT;
				remove();
				leftFaceFish();
			
			}
			remove();   //remove the former fish image
			if (direction == 1) rightFaceFish();
			else leftFaceFish();//draw the new fish with the new coordinates.
		
		}
	}

		


