import uwcse.graphics.*;
import uwcse.animation.*;

import java.awt.Color;

public class Ship implements Actor {
	// Not actually a ship
	
	private Polygon metronome,hull,leftsail,rightsail;
	private Triangle flag;
	
	
	private int rtime = 0; // Time elapsed
	private double rdelta = 0.0; // Acceleration
	private final double R_VELOCITY = .5; // Top speed in px/frame
	private final double R_TICK = 30; // How many frames it takes to reach pi (one half-cycle)
	
	public Ship() {
		
		// Create our metronome for testing purposes
		//Draw the mast.
		metronome = new Polygon(Color.BLACK, true);
			metronome.addPoint(240, 200);
			metronome.addPoint(260, 200);
			metronome.addPoint(260, 50);
			metronome.addPoint(240, 50);
			
		
		//Draw the hull
		hull = new Polygon(Color.YELLOW,true);
			hull.addPoint(100, 200);
			hull.addPoint(380, 200);
			hull.addPoint(290, 250);
			hull.addPoint(220, 250);
		
		//Draw the left sail
		rightsail = new Polygon(Color.CYAN,true);
			rightsail.addPoint(260, 50);
			rightsail.addPoint(260, 150);
			rightsail.addPoint(320, 150);
		
		//draw the right sail
		leftsail = new Polygon(Color.CYAN,true);
			leftsail.addPoint(240, 50);
			leftsail.addPoint(240, 150);
			leftsail.addPoint(180, 150);
		
		//Draw the flag
		flag = new Triangle(260,50,260,30,290,45,Color.green,true);
		
		
	}
	
	public void addTo(GWindow gw) {
		
		// Simple wrapping
		metronome.addTo(gw);
		hull.addTo(gw);
		flag.addTo(gw);
		rightsail.addTo(gw);
		leftsail.addTo(gw);
		
	}
	
	public void removeFromWindow() {
		
		// Simple wrapping
		metronome.removeFromWindow();
	}
	
	public void doAction(Stage stage) {
		
		rtime++; // Increment stage time
		
		rdelta = Math.cos((float)(rtime) / R_TICK); // Change velocity on cosine wave
		
		metronome.rotateAround(250, 250, rdelta * R_VELOCITY); // Rotate by velocity
		hull.rotateAround(250, 250, rdelta*R_VELOCITY);
		flag.rotateAround(250, 250, rdelta*R_VELOCITY);
		leftsail.rotateAround(250, 250, rdelta*R_VELOCITY);
		rightsail.rotateAround(250, 250, rdelta*R_VELOCITY);
	}
}
