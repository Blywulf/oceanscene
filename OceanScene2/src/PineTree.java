import java.awt.Color;
import uwcse.graphics.*;

/**
 * A class to draw a pine tree
 */

public class PineTree {

	// The graphics window this Tree belongs to
	private GWindow window;
	private Color leafcolor;
	private int x;
	private int y;
	/**
	 * Creates a PineTree
	 * 
	 * @param x
	 *            the x coordinate of the tree location (lower left corner of
	 *            the tree trunk)
	 * @param y
	 *            the y coordinate of the tree location
	 * @param window
	 *            the graphics window this Tree belongs to
	 */
	public PineTree(int x, int y, GWindow window) {
		this.x = x;
		this.y = y;
		this.window = window;
		this.leafcolor = Color.GREEN;
		this.drawPineTree(x, y);
	}

	/**
	 * Draws a pine tree at the location x,y
	 * 
	 * @param x
	 *            the x coordinate of the lower left corner of the tree trunk
	 * @param y
	 *            the y coordinate of the lower left corner of the tree trunk
	 */
	private void drawPineTree(int x, int y) {
		// a pine tree is a brown rectangle (the trunk) and 3 green triangles
		// (the foliage)
		// the code below has the positioning already done
		/*
		 * // trunk new Rectangle(x,y,20,60,Color.black,true); // foliage new
		 * Triangle
		 * (x-30,y+30,x+10,y-10,x+50,y+30,this.currentFoliageColor,true); new
		 * Triangle
		 * (x-25,y+30,x+10,y-10,x+45,y+30,this.currentFoliageColor,true); //
		 * then do a moveBy(0,-20); new
		 * Triangle(x-20,y+30,x+10,y-10,x+40,y+30,this
		 * .currentFoliageColor,true); // then do a moveBy(0,-40);
		 */
		Rectangle trunk = new Rectangle(x,y,20,60,Color.black,true);
		Triangle leaf1 = new Triangle(x-30,y+30,x+10,y-10,x+50,y+30,this.leafcolor,true);
		Triangle leaf2 = new Triangle(x-25,y+30,x+10,y-10,x+45,y+30,this.leafcolor,true);
		leaf2.moveBy(0,-20);
		Triangle leaf3 = new Triangle(x-20,y+30,x+10,y-10,x+40,y+30,this.leafcolor,true);
		leaf3.moveBy(0,-40);
		window.add(trunk);
		window.add(leaf1);
		window.add(leaf2);
		window.add(leaf3);
		
	}

	/**
	 * Changes the color of the tree
	 */
	public void flipColor() {
		if (this.leafcolor.equals(Color.GREEN)){
			this.leafcolor = Color.RED;
		}
		else {this.leafcolor = Color.GREEN;}
		
		this.drawPineTree(x, y);
		

	}

	// add a method below to switch the color of the foliage
	// between green and red

}