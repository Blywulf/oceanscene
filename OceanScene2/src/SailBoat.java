import java.awt.Color;

import uwcse.graphics.*;
/**
 * A sailboat in a graphics window
 */

public class SailBoat {

	// The graphics window the boat belongs to
	private GWindow window;

	// The location of the boat
	private int x;

	private int y;

	// Scale of the drawing of the boat
	private double scale;
	
	//Shapes
	private Triangle leftsail,rightsail,flag;

	/**
	 * Draws a sailboat in a graphics window
	 * 
	 * @param x
	 *            the x coordinate of the location of the boat
	 * @param y
	 *            the y coordinate of the location of the boat
	 * @param window
	 *            the graphics window where the boat is displayed
	 * @param window
	 *            the graphics window the boat belongs to
	 */
	public SailBoat(int x, int y, double scale, GWindow window) {
		// Initialize the instance fields
		this.x = x;
		this.y = y;
		this.scale = scale;
		this.window = window;
		
		// Draw the boat
		this.drawsailboat();
	}

	/**
	 * Draws this sailboat
	 */
	private void drawsailboat() {
		int r = (int)(Math.random() * 256);//random int red value
		int g = (int)(Math.random() * 256);//random int green value
		int b = (int)(Math.random() * 256);	//random int  blue value
		Color sailcolor = new Color(r,g,b);
		
		//Draw a mast
		int mastheight = (int)(40 * scale);
		int mastwidth = (int)(3 * scale);
		Rectangle mast = new Rectangle(x,y,mastwidth,mastheight , Color.BLACK, true);
		window.add(mast);
		
		// Draw the left sail.
		int vertexAX = this.x;
		int vertexAY = this.y;
		int vertexBX = this.x;
		int vertexBY = (int)(this.y +  (mastheight-5));
		int vertexCX = (int)(this.x - (20*scale)-10);
		int vertexCY = vertexBY;
		leftsail = new Triangle(vertexAX, vertexAY, vertexBX, vertexBY, vertexCX, vertexCY, sailcolor, true);
		window.add(leftsail);
		
		//Draw the right sail
		int vertexDX = this.x + mastwidth;
		int vertexDY = this.y;
		int vertexEX = vertexDX;
		int vertexEY = (int)(this.y +  (mastheight-5));
		int vertexFX = (int)(this.x + (20*scale)+10);
		int vertexFY = vertexBY;
		rightsail = new Triangle(vertexDX, vertexDY, vertexEX, vertexEY, vertexFX, vertexFY, sailcolor, true);
		window.add(rightsail);
		
		//Draw the flag
		int f =(int)(Math.random()*1); //If f == 2 then the flag with point the other direction
		int vertexGX = this.x + mastwidth;
		if (f == 2){ vertexGX = this.x;} 
		int vertexGY = this.y;
		int vertexHX = vertexGX;
		int vertexHY = (int)(this.y -  (mastheight/4));
		int vertexIX = (int)(this.x + (20*scale));
		if (f == 2){ vertexIX = (int)(this.x - (20*scale));}
		int vertexIY = vertexAY;
		flag = new Triangle(vertexGX, vertexGY, vertexHX, vertexHY, vertexIX, vertexIY, Color.BLACK, true);
		window.add(flag);
		
		//Draw the hull
		int r2 = (int)(Math.random() * 256);//random int red value
		int g2 = (int)(Math.random() * 256);//random int green value
		int b2 = (int)(Math.random() * 256);	//random int  blue value
		Color boatcolor = new Color(r2,g2,b2);
		
		Polygon hull = new Polygon(boatcolor,true);
		int polyvertexAX = (int)(x + (40* scale));
		int polyvertexAY = (int)(y - (scale* mastheight ));
		int polyvertexBX = (int)(x - (40* scale));
		int polyvertexBY = (int)(y - (scale* mastheight));
		int polyvertexCX = (int)(polyvertexBX +(25*scale));
		int polyvertexCY = (int)(polyvertexBY +(20*scale));
		int polyvertexDX = (int)(polyvertexAX -(25*scale));
		int polyvertexDY = (int)(polyvertexCY);
		hull.addPoint(polyvertexAX,polyvertexAY);
		hull.addPoint(polyvertexBX,polyvertexBY);
		hull.addPoint(polyvertexCX, polyvertexCY);
		hull.addPoint(polyvertexDX, polyvertexDY);
		int hullWidth = hull.getWidth();
		int hullX = (int)(x -(hullWidth/2));
		int hullY = (int)(y + mastheight);
		hull.moveTo(hullX,hullY);
		window.add(hull);	
		
	}
}