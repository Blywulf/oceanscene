import java.awt.Color;

import uwcse.graphics.*;

/**
 * A star in a graphics window
 */

public class Star {

	// The graphics window the star belongs to
	private GWindow window;

	// The location of the center of the star
	private int x;

	private int y;

	// Scale of the drawing of the star
	private double scale;
	
	//Star color
	private Color starcolor;
	
	private Line line1,line2,line3,line4,line5;

	/**
	 * Draws a star in a graphics window
	 * 
	 * @param x
	 *            the x coordinate of the center of the star
	 * @param y
	 *            the y coordinate of the center of the star
	 * @param scale
	 *            the scale of the drawing of the star
	 * @param window
	 *            the graphics window the star belongs to
	 */
	public Star(int x, int y, double scale, GWindow window) {
		// Initialize the instance fields
		this.x = x;
		this.y = y;
		this.window = window;
		this.scale = scale;

		// Draw the star
		this.drawStar();
	}

	/**
	 * Draws this star (complete this method to make a better star)
	 */
	public void drawStar() {
		
		// color of the star
		int r = (int)(Math.random() * 256);//random int red value
		int g = (int)(Math.random() * 256);//random int green value
		int b = (int)(Math.random() * 256);	//random int  blue value
		starcolor = new Color(r,g,b);
		
		// Size of the star
		int starSize = (int) (this.scale * 10);
		
		// Draw the star
		line1 = new Line(this.x, this.y - (starSize) , this.x, this.y
				+ starSize / 2, starcolor);
		
		window.add(line1);
		
		line2 = new Line(this.x - starSize / 2, this.y, this.x + starSize
				/ 2, this.y, starcolor);
		window.add(line2);
		line3 = new Line(this.x - (starSize )/ 2, this.y, this.x + starSize
				/ 2, this.y, starcolor);
		line3.rotateAround(this.x, this.y, 40);
		window.add(line3);
		
		line4 = new Line(this.x - starSize / 2, this.y, this.x + starSize
				/ 2, this.y, starcolor);
		line4.rotateAround(this.x, this.y, 135);
		window.add(line4);
		
		line5 = new Line(this.x, this.y - (starSize-11) , this.x, this.y
				+ starSize / 2, starcolor);
		line5.rotateAround(this.x, this.y, 90);
		window.add(line5);	
	}
	public void remove(){
		window.remove(line1);
		window.remove(line2);
		window.remove(line3);
		window.remove(line4);
		window.remove(line5);
	}
	public void twinkle(){
			remove();
			drawStar();
				
	}
	
	
	
	
	
	
	
}