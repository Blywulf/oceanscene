import java.awt.Color;

import uwcse.graphics.*;

/**
 * A star in the graphics window
  
 */

public class Island {
	// The graphics window the star belongs to
		private GWindow window;

		// The location of the center of the star
		private int x;

		private int y;

		// Scale of the drawing of the star
		private double scale;
		
		private Rectangle tree;
		
		// Moving objects
		private Polygon leaf1,leaf2;

		private Oval stalk;
		
		
		/**
		 * Draws a island in a graphics window
		 * 
		 * @param x
		 *            the x coordinate of the center of the island
		 * @param y
		 *            the y coordinate of the center of the island
		 * @param scale
		 *            the scale of the drawing of the island
		 * @param window
		 *            the graphics window the island belongs to
		 */
		public Island(int x, int y, double scale, GWindow window) {
			// Initialize the instance fields
			this.x = x;
			this.y = y;
			this.window = window;
			this.scale = scale;

			// Draw the island
			this.drawIsland();
		
		}
		public void drawIsland(){
			
			int pxscale = 10;
			
			//Draw the land
			int arcX = x;
			int arcY = y;
			int arcWidth =(int)( 50 + (scale*pxscale));
			int arcHeight = (int)(25+ (scale*pxscale));
			Arc land = new Arc(arcX, arcY, arcWidth, arcHeight, -180, -180, Color.GREEN, true);
			land.rotateAround(arcX, arcY, 180);
			
			
			
			//Draw tree trunk
			int treeX = land.getCenterX();
			int treeY = (int)(land.getCenterY()-(arcHeight*1.7+(5*scale)));
			int treeWidth = (int)(1 + (scale*pxscale));
			int treeHeight = (int)(arcHeight*1.7+(5*scale));
			Color treeColor = new Color(139,69,19);
			tree = new Rectangle(treeX,treeY,treeWidth,treeHeight,treeColor,true);
			window.add(tree);
			window.add(land);
		
			
			//Draw the leaves
			
			stalk = new Oval(treeX,treeY,treeWidth,treeWidth,Color.GREEN,true);
			int leafHeight = (int)(10+(scale+pxscale));
			int leafWidth = (int)(6*pxscale *(scale*0.7));
			int leafX = (int)(stalk.getCenterX());
			int leafY = (int)(stalk.getCenterY()-10);
			int leaf2X = (int)(leafX-leafWidth);
			leaf1 = new Polygon(leafX,leafY,leafWidth,leafHeight,-180,-180, Color.GREEN,true);
			leaf2 = new Polygon(leaf2X,leafY,leafWidth,leafHeight,-180,-180, Color.GREEN,true);
			window.add(stalk);
			window.add(leaf1);
			window.add(leaf2);
			
			
		}
		
		public void remove(){
			//Remove the leaves
			window.remove(leaf1);
			window.remove(leaf2);
			//add them back
			window.add(leaf1);
			window.add(leaf2);
			
		}
		
		private int rotate = 5;
		
		public void flapInWind(){
			remove();
			int rotateX = (int)(leaf1.getX());
			int rotateY = (int)(leaf1.getY() + leaf1.getHeight());
			int l2RotateX = (int)(leaf2.getX() + leaf2.getWidth());
			int l2RotateY = (int)(leaf2.getY() +leaf2.getHeight());
			// this.leaf1.rotateAround(rotateX, rotateY, 5);
			leaf1.rotateAround(stalk.getCenterX(),stalk.getCenterY() - 10 , 30);
			leaf1.moveTo(stalk.getCenterX(), rotateY);
			this.rotate = -this.rotate;
	
		}

}
