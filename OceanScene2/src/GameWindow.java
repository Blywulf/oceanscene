/*
 * Moo.
 */


import uwcse.graphics.*;
import uwcse.animation.*;

public class GameWindow {
	
	// Game window
	private GWindow screen;
	private Stage scene;
	
	public GameWindow() {
		screen = new GWindow();
		screen.setExitOnClose();
		screen.addEventHandler(new Controller());
		
		scene = new Stage(screen);
		scene.quitWhenWindowClosed();
		scene.makeAnimationRoundsAtomic();
		scene.setTimeForOneRound(16);
		
		scene.addActor(new Ship());
		
		scene.animate();
	}
	
	
	public static void main(String[] args) {
		new GameWindow();
		
	}
}
